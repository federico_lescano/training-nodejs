arrayToRotate = [1, 2, 3, 4, 5];

const leftRotation = (arr, d) => {

    while (d) {
        arr.push(arr.shift());

        console.log('\nVuelta numero: ' + d)
        console.log(arr);

        d--;
    }

    return arr;
}

console.log('\nOriginal Array: ' + arrayToRotate);
const arrayRotated = leftRotation(arrayToRotate, 4);
console.log('\nFinal Array: ' + arrayRotated);

module.export = leftRotation;