const fs = require('fs')

fs.readFile('./cashflow.txt', 'utf8', (err, data) => {
    if (err) {
        console.error(err)
        return
    }

    let lines = data.toString().split('\r\n')
    lines.shift();

    let total = 0;

    lines.forEach(line => {

        total += parseInt(line.substring(line.indexOf('S') + 1));

    });

    if (total > 0) {

        fs.appendFile('./cashflow.txt', '\nTotal amount: ' + total.toString(), (err) => {
            if (err) return console.error(err);
            console.log('Process successful');
        });
    }
})