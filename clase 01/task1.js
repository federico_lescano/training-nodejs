students = [{
        name: 'JOHN',
        lastname: 'DOE',
        score: 4
    },
    {
        name: 'EVELYN',
        lastname: 'JACKSON',
        score: 8
    },
    {
        name: 'JAMES',
        lastname: 'SHAW',
        score: 2
    },
    {
        name: 'Raul',
        lastname: 'BrAvO',
        score: 3
    }
];

// Return only approved students
const approvedStudents = (studentsArray, minScore) => {

    let studentsOk = [];

    studentsArray.forEach(student => {
        if (student.score >= minScore)
            studentsOk = studentsOk.concat(student)
    });

    return studentsOk;
}

// Capitalize name and lastname for each element in the array
const capitalizeArray = (studentsApprovedArray) => {

    for (const student of studentsApprovedArray) {
        student.name = student.name.toLowerCase().charAt(0).toUpperCase() + student.name.toLowerCase().slice(1);
        student.lastname = student.lastname.toLowerCase().charAt(0).toUpperCase() + student.lastname.toLowerCase().slice(1);
    }

    return studentsApprovedArray;
}

// Sort ascending array by score  
const sortArray = (arr) => arr.sort((a, b) => a.score - b.score);

let result1 = approvedStudents(students, 3);
let result2 = capitalizeArray(result1);
let finalResult = sortArray(result2);

console.log(finalResult);